import logging
import traceback
import asyncio

logger = logging.getLogger(__name__)



def run(fut):


    async def log_func():

        try:
            await fut

        except Exception as e:

            logger.error(traceback.format_exc())

            raise e

    asyncio.ensure_future(log_func())