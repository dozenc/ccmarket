import time
import json
import hashlib
import logging

from libex.base_market_listener import BaseMarketListener

logger = logging.getLogger(__name__)


class MarketWsListener(BaseMarketListener): 
    def __init__(self,ex,listeners):
        super(MarketWsListener, self).__init__(ex)
       
        self._listeners = listeners

    def on_sent(self,plain):
        
        logger.debug(f"{self._exchange},send:{plain}")

        for listener in self._listeners:
            listener.on_sent(plain)

    def on_recv(self,plain):

        logger.debug(f"{self._exchange},recv:{plain}" )

        for listener in self._listeners:
            listener.on_recv(plain)
        
    def on_start(self):

        logger.info(f"{self._exchange},connected" )

        for listener in self._listeners:
            listener.on_start()

        
    def on_end(self):

        logger.info(f"{self._exchange},disconnect")

        for listener in self._listeners:
            listener.on_end()

    def on_resolve(self,coin,currency,channel,cid,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{channel},{cid},{ts},{detail}" )

        for listener in self._listeners:
            listener.on_resolve(coin,currency,channel,cid,ts,detail)


    def on_ticker(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )

        for listener in self._listeners:
            listener.on_ticker(coin,currency,ts,detail)

    def on_trade(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )

        for listener in self._listeners:
            listener.on_trade(coin,currency,ts,detail)

    def on_book_update(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )

        for listener in self._listeners:
            listener.on_book_update(coin,currency,ts,detail)

    def on_book_snapshot(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )


        for listener in self._listeners:
            listener.on_book_snapshot(coin,currency,ts,detail)

    def on_book5(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )

        for listener in self._listeners:
            listener.on_book5(coin,currency,ts,detail)


    def on_book10(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )

        for listener in self._listeners:
            listener.on_book10(coin,currency,ts,detail)


    def on_book20(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )

        for listener in self._listeners:
            listener.on_book20(coin,currency,ts,detail)


    def on_candle1h(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )


        for listener in self._listeners:
            listener.on_candle1h(coin,currency,ts,detail)

    def on_candle5m(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )


        for listener in self._listeners:
            listener.on_candle5m(coin,currency,ts,detail)

    def on_candle5m_snapshot(self,coin,currency,ts,detail):

        logger.debug(f"{self._exchange},{coin},{currency},{ts},{detail}" )


        for listener in self._listeners:
            listener.on_candle5m_snapshot(coin,currency,ts,detail)


    def on_market_event(self,event):

        logger.debug(f"{self._exchange} , {event}")

        for listener in self._listeners:
            listener.on_market_event(event)


    def on_flag(self,ts):

        logger.debug(f"running flag,{self._exchange},{ts}" )

        for listener in self._listeners:
            listener.on_flag(ts)

