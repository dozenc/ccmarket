import sys
from configparser import ConfigParser
import logging

from .const import SUPPORTED_EXCHANGES 

logger = logging.getLogger(__name__)

this = sys.modules[__name__]

class Config(ConfigParser):

    def __init__(self):
        super(Config, self).__init__()

        self.resolved = None

    def read(self,config_file):
        super().read(config_file)

        logger.info(f"read and resolve config file:{config_file}")

        self.resolved = self.as_dict()

        self.resolved['app']['redis_host'] = self['app']['redis_host']
        self.resolved['app']['redis_port'] = int(self['app']['redis_port'])
        self.resolved['app']['redis_db'] = int(self['app']['redis_db'])

        self.resolved['file_listener']['switch'] = self['file_listener']['switch'] == 'on'
        self.resolved['file_listener']['file_root_path'] = self['file_listener']['file_root_path']
        
        self.resolved['status_listener']['switch'] = self['status_listener']['switch'] == 'on'
        self.resolved['status_listener']['connect_status_minutes'] = int(self['status_listener']['connect_status_minutes'])
        
        self.resolved['redis_listener']['switch'] = self['redis_listener']['switch'] == 'on'
        self.resolved['redis_listener']['redis_host'] = self['redis_listener']['redis_host']
        self.resolved['redis_listener']['redis_port'] = int(self['redis_listener']['redis_port'])
        self.resolved['redis_listener']['redis_db'] = int(self['redis_listener']['redis_db'])
        
        for ex in SUPPORTED_EXCHANGES:
            self.resolved[ex]['switch'] = self[ex]['switch'] == 'on'
            self.resolved[ex]['symbols'] = [ tuple(x.strip().split('_')) for x in self[ex]['symbols'].split(',')]
            self.resolved[ex]['channels'] = [x.strip() for x in self[ex]['channels'].split(',')] 
            self.resolved[ex]['currencies'] = [ x.strip() for x in self[ex]['currencies'].split(',')]
            self.resolved[ex]['stable'] = self[ex]['stable']
            
    def as_dict(self):
        d = dict(self._sections)

        for k in d:
            d[k] = dict(self._defaults, **d[k])
            d[k].pop('__name__', None)
        return d

def load(path):
    this.config = Config()
    this.config.read(path)

    this.resolved = this.config.resolved
