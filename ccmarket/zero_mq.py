import asyncio
import zmq
import zmq.asyncio
import traceback
import json

import logging

logger = logging.getLogger(__name__)

url = "tcp://*:5551"
ctx = zmq.asyncio.Context()


class ZeroMQ(object):
    """docstring for ZeroMQ"""

    def __init__(self, keepalive_serv):
        super(ZeroMQ, self).__init__()

        self._keepalive_serv = keepalive_serv

    async def serv(self):
        sock = ctx.socket(zmq.REP)
        sock.bind(url)
        while True:

            try:
                logger.info('ready for recv ...')
                msg = await sock.recv()  # waits for msg to be ready
                ret = 'error'
                
                logger.info('recv: %s', msg)

                j = json.loads(msg)

                method = j.get('method')
                args = j.get('args')

                if method == 'ping':
                    ret = self._keepalive_serv.run(args)


                logger.info('send: %s', ret)
                await sock.send_string(ret)

            except Exception as e:
                logger.error(traceback.format_exc())
