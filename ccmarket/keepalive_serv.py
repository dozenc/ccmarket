
class KeepAliveServ:
    def __init__(self):
        pass

    def gather_info(self, args):
        exchange = args.get('last_restart')
        if not exchange:
            return "pong"
        else:
            return "pong pong"

    def run(self, args):
        return self.gather_info(args)

