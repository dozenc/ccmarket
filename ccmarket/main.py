import logging
import logging.config
import traceback
import sys
import asyncio
import json
from redis import StrictRedis

from libex import loader

from . import config

linux_logging_path = sys.prefix + '/etc/ccmarket/logging.ini'
linux_config_path = sys.prefix + '/etc/ccmarket/config.ini'

local_logging_path = './logging.dev.ini'
local_config_path = './config.ini'


async def app_live():
    logger = logging.getLogger(__name__)

    try:

        redis_host = config.resolved['app']['redis_host']
        redis_port = config.resolved['app']['redis_port']
        redis_db = config.resolved['app']['redis_db']

        redis_client = StrictRedis(host=redis_host, port=redis_port, db=redis_db, decode_responses=True)

        while True:
            logger.debug('redis set ccmarket_live')
            redis_client.setex('ccmarket_live', 5, 'live')
            redis_client.publish('ccmarket_live', json.dumps({'ccmarket': True}))

            await asyncio.sleep(3)
    except Exception as e:
        logger.error(traceback.format_exc())
        raise e


def main(config_path, logging_path):
    import os

    if not os.path.exists('logs'):
        os.makedirs('logs')

    logging.config.fileConfig(logging_path)

    print('loggingconfig', logging.config)

    import importlib
    import asyncio
    import time
    import os
    import re
    from datetime import datetime

    from .common.timer import Timer
    from .const import SUPPORTED_EXCHANGES
    from .market_ws_listener import MarketWsListener
    from .listeners.file_listener import FileListener
    from .listeners.status_listener import StatusListener
    from .listeners.redis_listener import RedisListener

    from .common import logged_async
    from .keepalive_serv import KeepAliveServ
    from .zero_mq import ZeroMQ

    config.load(config_path)

    logger = logging.getLogger(__name__)

    ex_symbols = {}
    ex_channels = {}
    ex_market_ws_class = {}
    ex_currencies = {}
    ex_stablecoins = {}

    loop = asyncio.get_event_loop()

    for ex in SUPPORTED_EXCHANGES:
        if not config.resolved[ex]['switch']:
            continue

        ex_symbols[ex] = config.resolved[ex]['symbols']
        ex_channels[ex] = config.resolved[ex]['channels']
        ex_currencies[ex] = config.resolved[ex]['currencies']
        ex_stablecoins[ex] = config.resolved[ex]['stable']

        # 加载各交易所的MarketWs类（MarketWsSdk）
        # market_ws_str = f".exchanges.{ex}.market_ws"
        # logger.debug(f"market_ws_str:{ market_ws_str}")
        # market_ws_m = importlib.import_module(market_ws_str,__package__) 
        # market_ws_class = getattr (market_ws_m, 'MarketWs')
        # ex_market_ws_class[ex] = market_ws_class

        listeners = []
        if config.resolved['status_listener']['switch']:
            listeners.append(StatusListener(ex, config.resolved['status_listener']['connect_status_minutes']))
        if config.resolved['file_listener']['switch']:
            listeners.append(FileListener(ex, config.resolved['file_listener']['file_root_path']))
        if config.resolved['redis_listener']['switch']:
            listeners.append(RedisListener(ex, config.resolved['redis_listener']['redis_host'],
                                           config.resolved['redis_listener']['redis_port'],
                                           config.resolved['redis_listener']['redis_db']))

        market_ws = loader.load_market_ws(ex, ex_symbols[ex], ex_channels[ex], MarketWsListener(ex, listeners),
            ex_currencies[ex] ,ex_stablecoins[ex])

        logger.info(f"{ex},will add task to asyncio")

        loop.create_task(market_ws.task())

    keepalive_serv = KeepAliveServ()
    zero_task = ZeroMQ(keepalive_serv)
    
    logged_async.run(zero_task.serv())
    # loop.create_task(zero_task.serv())

    loop.create_task(app_live())

    loop.run_forever()


def linux():
    main(linux_config_path, linux_logging_path)


if __name__ == '__main__':
    main(local_config_path, local_logging_path)
