import time
import json
import hashlib
import logging
from redis import StrictRedis

from ..common.timer import Timer
from libex.base_market_listener import BaseMarketListener
from libex.events import EventType

logger = logging.getLogger(__name__)
prefix = 'ccmarket'


def update_books(books,coin,currency,asks,bids,timestamp):

    logger.debug(f"update depth,{coin}_{currency}, asks:{asks} , bids:{bids} , timestamp:{timestamp}\n")

    key = "%s_%s" % (coin,currency)

    
    if asks:
        curr_asks = books.setdefault(key,{}).setdefault('asks',[])
    
        for item in asks:

            arr = [n for n in curr_asks if n[0] != item[0]]

            if item[1] != '0':
                arr.append(item)

            curr_asks = arr

        sorted_asks = sorted(curr_asks, key=lambda a: a[0])[0:200]  # 按每个元素的第一个数据排序
        books.setdefault(key,{})['asks'] = sorted_asks

    if bids:        
        curr_bids = books.setdefault(key,{}).setdefault('bids',[])

        for item in bids: 

            arr = [n for n in curr_bids if n[0] != item[0]]

            if item[1] != '0':
                arr.append(item)

            curr_bids = arr

    
        sorted_bids = sorted(curr_bids, key=lambda a: a[0],reverse=True)[0:200]  # 按每个元素的第一个数据排序
        books.setdefault(key,{})['bids'] = sorted_bids
      
    books.setdefault(key,{})['timestamp'] = timestamp


class RedisListener(BaseMarketListener): 
    def __init__(self,ex ,redis_host, redis_port ,redis_db):
        super(RedisListener, self).__init__(ex)

        self._redis = StrictRedis(host=redis_host, port=redis_port , db=redis_db ,decode_responses=True)

        self._books = {}

        self._publish = True

    def on_sent(self,plain):
        pass

    def on_recv(self,plain):
        pass

    def on_start(self):
        pass
    
    def on_end(self):
        pass

    def on_resolve(self,coin,currency,channel,cid,ts,detail):
        pass

    def on_flag(self,ts):

        pass

    # def on_ticker(self,coin,currency,ts,detail):
    #     key = f"{prefix}_{self._exchange}_{coin}_{currency}_ticker"
    #     self._redis.set(key , json.dumps(detail))
        
    #     if self._publish:
    #         self._redis.publish(key, json.dumps(detail))

    def on_trade(self,coin,currency,ts,detail):
        key = f"{prefix}_{self._exchange}_{coin}_{currency}_trade"
        self._redis.lpush(key , json.dumps(detail))
        self._redis.ltrim(key, 0,20)

        if self._publish:
            self._redis.publish(key, json.dumps(detail))

    def on_book_update(self,coin,currency,ts,detail):

        symbol  = f'{coin}_{currency}'

        update_books(self._books , coin,currency , detail.get('asks'),detail.get('bids'),ts)
    
        key = f"{prefix}_{self._exchange}_{symbol}_book"
        self._redis.set(key, json.dumps(self._books[symbol]))

        if self._publish:
            self._redis.publish(key , json.dumps(detail))
    
    def on_book_snapshot(self,coin,currency,ts,detail):
        
        symbol  = f'{coin}_{currency}'

        self._books.setdefault(symbol,{})['asks'] = detail.get('asks')
        self._books.setdefault(symbol,{})['bids'] = detail.get('bids')
        self._books.setdefault(symbol,{})['timestamp'] = ts

        key = f"{prefix}_{self._exchange}_{symbol}_book"
        self._redis.set(key, json.dumps(self._books[symbol]))

        if self._publish:
            self._redis.publish(key , json.dumps(detail))

    
    def on_stateless(self,name,coin,currency,ts,detail):

        symbol  = f'{coin}_{currency}'

        key = f"{prefix}_{self._exchange}_{symbol}_{name}"

        self._redis.set(key, json.dumps(detail))

        if self._publish:
            self._redis.publish(key, json.dumps(detail))


    # def on_candle1h(self,coin,currency,ts,detail):
        
    #     key = f"{prefix}_{self._exchange}_{coin}_{currency}_candle1h"

    #     last = self._redis.lrange(key, -1 , -1)

    #     logger.debug(f'last:{last}, detail:{detail}')

    #     if len(last) > 0 and json.loads(last[0])[0] == detail[0]:
    #         self._redis.lset(key,-1,json.dumps(detail))
    #     else:
    #         self._redis.rpush(key,json.dumps(detail))

        
    #     self._redis.ltrim(key, -360, -1)   
        
    #     if self._publish:
    #         self._redis.publish(key, json.dumps(detail))


    def on_candle_update(self, name,coin,currency,ts,detail):
        
        key = f"{prefix}_{self._exchange}_{coin}_{currency}_{name}"

        last = self._redis.lrange(key, -1 , -1)

        logger.debug(f'last:{last}, detail:{detail}')

        if len(last) > 0 and json.loads(last[0])[0] == detail[0]:
            self._redis.lset(key,-1,json.dumps(detail))
        else:
            if len(last) > 0 :
                if 'candle5m' == name and ( detail[0] - json.loads(last[0])[0]) > 5*60  :
                    self._redis.delete(key)
                if 'candle1h' == name and ( detail[0] - json.loads(last[0])[0]) > 60*60  :
                    self._redis.delete(key)
            self._redis.rpush(key,json.dumps(detail))
            
        
        self._redis.ltrim(key, -1000, -1)   
        
        if self._publish:
            self._redis.publish(key, json.dumps(detail))
    
    def on_candle_snapshot(self,name,coin,currency,ts,detail):
        
        key = f"{prefix}_{self._exchange}_{coin}_{currency}_{name}"

        self._redis.delete(key)

        for item in detail:
            self._redis.lpush(key,json.dumps(item))
            
        if self._publish:
            self._redis.publish(key, json.dumps(detail))
            
    def _on_rates(self,event):
        key = f"{prefix}_{self._exchange}_{event.name}"

        self._redis.set(key, json.dumps(event.message))

        if self._publish:
            self._redis.publish(key, json.dumps(event.message))


    def on_market_event(self,event):
        key = f"{prefix}_{self._exchange}_{event.coin}_{event.currency}_{event.name}"

        if event.name in ['candle5m','candle1h'] :
            if EventType.snapshot == event.type  :

                self.on_candle_snapshot(event.name,event.coin,event.currency,None,event.message)

            elif EventType.update == event.type:
                self.on_candle_update(event.name,event.coin,event.currency,None,event.message)

        elif event.name in ['book5','book10','book20','ticker']:
            self.on_stateless(event.name,event.coin,event.currency, None, event.message)

        elif event.name in ['rates']:
            self._on_rates(event)
        elif event.name in ['book'] :
            if EventType.snapshot == event.type:
                self.on_book_snapshot(event.coin,event.currency,None,event.message)
            elif EventType.update == event.type:
                self.on_book_update(event.coin,event.currency,None,event.message)

        elif event.name in ['trade']:
            self.on_trade(event.coin,event.currency,None,event.message)
        else:
            logger.warning('unhandled event: %s' , event);