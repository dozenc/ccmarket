# ccmarket

Crypto Currency Market,Realtime Market of Crypto Currency Exchanges.

# Environment

# Install
Currently,Application support Linux and Mac.

python3 -m pip install ccmarket


# Config
### logging config:

/usr/local/etc/ccmarket/logging.ini

By default,Log file will output to /usr/local/var/log/ccmarket/ .

### application config:

/usr/local/etc/ccmarket/config.ini

You could config exchanges and symbols in this config file.

# Run
Open terminal and input following commamnd:
ccmarket